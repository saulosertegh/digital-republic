## Nature of the Project

This project was built foloowing instructions in order to fulfill a coding test to join Digital Republic

## Installation Instructions

You can clone the repo and simply run an npm install in the project direactory

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

## Deploy

I canse you don´t wanna go through all that, you can just access the link below and check it out:

https://digital-republic-codetest.netlify.app