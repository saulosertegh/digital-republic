import React, { useState, createContext, useContext } from 'react'
import { WALLS_AMOUNT } from './Rules'

const dataContext = createContext()

export const NEW_WALL = (id) => {
    return {
      id,
      size: 0,
      sizeError: false,
      availableSize: 0,
      sizeDisError: false,
      length: 0,
      height: 0,
      heightError: false,
      walls: 0,
      wallsError: false,
      doors: 0,
      doorsError: false,
      paint: 0,
      ok: false,
    }
  }

export default function Walls({ children }) {

const [totalWall, setTotalWall] = useState(0)
const [walls, setWalls] = useState([])

  if (walls.length < WALLS_AMOUNT)
    setWalls(walls => [...walls, NEW_WALL(walls.length)])

  return (
    <dataContext.Provider value={{ walls, setWalls, totalWall, setTotalWall }}>
        {children}
    </dataContext.Provider>
  )
}

export function useWallData() {
    const contextWall = useContext(dataContext);
    if (!contextWall) throw new Error("useParedeData must be used within a Walls provider");
    const { walls, setWalls, totalWall, setTotalWall } = contextWall;
    return { walls, setWalls, totalWall, setTotalWall }
  }
