export const WALLS_AMOUNT = 4;

export const MAXIMUM_SIZE = 15;

export const MINIMUM_SIZE = 1;

export const MINUMUM_HEIGHT = 2.20;

export const PAINT_LITER = 5;

export const WINDOW_SIZE = 2.40;

export const DOOR_SIZE = 1.52;

export const MINIMUM_LENGTH = 2;

export const CANS = [
    {
        can: 1,
        amount: 0.5
    },
    {
        can: 2,
        amount: 2.5
    },
    {
        can: 3,
        amount: 3.6
    },
    {
        can: 4,
        amount: 18
    }
]