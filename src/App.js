import './App.css';
import Body from './template/body/Body';
import WallsData from './config/Walls';

function App() {
  return (
    <div className="App">
     <WallsData>
       <Body />
     </WallsData>
    </div>
  );
}

export default App;
