import React, { useState } from "react";
import { useWallData } from "../../../config/Walls";
import { MINUMUM_HEIGHT, DOOR_SIZE } from "../../../config/Rules";
import { IconButton, Collapse } from "@material-ui/core";
import RemoveCircleIcon from "@material-ui/icons/RemoveCircle";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import CloseIcon from "@material-ui/icons/Close";
import Alert from "@material-ui/lab/Alert";
import "./door.css";

export default function Door({ wallId }) {
  const [open, setOpen] = useState(false);
  const { walls, setWalls } = useWallData();

  function addDoor() {
    if (
      walls[wallId].availableSize >= DOOR_SIZE &&
      walls[wallId].height >= MINUMUM_HEIGHT
    ) {
      setWalls([...walls], walls[wallId].doors++);
      setWalls([...walls], walls[wallId].availableSize -= DOOR_SIZE);
      setOpen(false);
    } else {
      setWalls([...walls], walls[wallId].doorsError = true);
      setOpen(true);
    }
  }

  function removeDoor() {
    if (walls[wallId].doors !== 0) {
      setWalls([...walls], walls[wallId].doors--);
      setWalls([...walls], walls[wallId].availableSize += DOOR_SIZE);
      setWalls([...walls], walls[wallId].doorsError = false);
      setOpen(false);
    }
  }

  return (
    <div className="add-item">
      {
        <Collapse in={open}>
          <Alert
            severity="warning"
            action={
              <IconButton
                aria-label="close"
                color="inherit"
                size="small"
                onClick={() => {
                  setOpen(false);
                }}
              >
                <CloseIcon fontSize="inherit" />
              </IconButton>
            }
          >
            Not enought space!
          </Alert>
        </Collapse>
      }
      <IconButton size="medium">
        <RemoveCircleIcon
          fontSize="inherit"
          className="sub"
          onClick={() => removeDoor()}
        />
      </IconButton>
      <span className="item">
        {walls[wallId].doors > 0 ? "Doors " : "Door "}[
        {walls[wallId].doors}]
      </span>
      <IconButton size="medium">
        <AddCircleIcon fontSize="inherit" className="add"
          onClick={() => addDoor()}
        />
      </IconButton>
      <br />
      <h6 className="door-size">{DOOR_SIZE}m²</h6>
    </div>
  );
}
