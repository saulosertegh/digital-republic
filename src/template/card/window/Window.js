import React, { useState } from 'react';
import { useWallData } from '../../../config/Walls';
import { WINDOW_SIZE, MINIMUM_LENGTH } from '../../../config/Rules';
import { IconButton, Collapse } from '@material-ui/core';
import RemoveCircleIcon from '@material-ui/icons/RemoveCircle';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import CloseIcon from '@material-ui/icons/Close';
import Alert from '@material-ui/lab/Alert';
import './window.css';


export default function Window({ wallId }) {
    const [open, setOpen] = useState(false);
    const { walls, setWalls } = useWallData();

    function addWindow() {
        if (walls[wallId].availableSize >= WINDOW_SIZE && walls[wallId].length >= MINIMUM_LENGTH) {
            setWalls([...walls], walls[wallId].windows++)
            setWalls([...walls], walls[wallId].availableSize -= WINDOW_SIZE)
            setWalls([...walls], walls[wallId].windowsError = false)
            setOpen(false)
          } else {
            setWalls([...walls], walls[wallId].windowsError = true)
            setOpen(true)
          }
    }

    function removeWindow() {
        if (walls[wallId].windows !== 0) {
          setWalls([...walls], walls[wallId].windows--)
          setWalls([...walls], walls[wallId].availableSize += WINDOW_SIZE)
          setWalls([...walls], walls[wallId].windowsError = false)
          setOpen(false)
        }
      }

  return (
    <div className='add-item'>
        {
        <Collapse in={open}>
          <Alert severity="warning"
            action={
              <IconButton
                aria-label="close" color="inherit" size="small"
                onClick={() => {
                  setOpen(false);
                }}
              >
                <CloseIcon fontSize="inherit" />
              </IconButton>
            }
          >
            Not enough space!
          </Alert>
        </Collapse>
      }
      <IconButton size="medium">
        <RemoveCircleIcon fontSize="inherit" className="sub"
          onClick={() => removeWindow()}
        />
      </IconButton>
      <span className="item">
        {(walls[wallId].windows > 0 ? 'Windows ' : 'Window ')}
      [{walls[wallId].windows}]
      </span>
      <IconButton size="medium">
        <AddCircleIcon fontSize="inherit" className="add"
          onClick={() => addWindow()}
        />
      </IconButton>
      <br />
      <h6 className="window-size">{WINDOW_SIZE.toFixed(2)}m²</h6>
    </div>
  )
}
