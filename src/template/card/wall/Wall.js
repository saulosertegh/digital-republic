import React from "react";
import { useWallData } from "../../../config/Walls";
import { MAXIMUM_SIZE, MINIMUM_SIZE, MINUMUM_HEIGHT } from "../../../config/Rules";
import { InputAdornment, Input, Collapse } from "@material-ui/core";
import Alert from "@material-ui/lab/Alert";
import "./wall.css";

export default function Wall({ wallId }) {
  const { walls, setWalls } = useWallData();

  function resetItems() {
    setWalls([...walls], (walls[wallId].windows = 0));
    setWalls([...walls], (walls[wallId].doors = 0));
    setWalls([...walls], (walls[wallId].windowsError = false));
    setWalls([...walls], (walls[wallId].doorsError = false));
  }

  function updateSquareMeters() {
    const totalSquareMeters = walls[wallId].length * walls[wallId].height;
    if (totalSquareMeters <= MAXIMUM_SIZE && totalSquareMeters >= MINIMUM_SIZE) {
      setWalls([...walls], walls[wallId].size = totalSquareMeters)
      setWalls([...walls], walls[wallId].availableSize = (totalSquareMeters) / 2)
      setWalls([...walls], walls[wallId].sizeError = false)
    } else {
      setWalls([...walls], walls[wallId].sizeError = true)
    }
  }

  function addHeight(size) {
    resetItems()
    if (size >= MINUMUM_HEIGHT) {
      setWalls([...walls], walls[wallId].height = parseFloat(size))
      setWalls([...walls], walls[wallId].heightError = false)
    } else {
      setWalls([...walls], walls[wallId].heightError = true)
    }
    updateSquareMeters()
  }

  function addLength(size) {
    resetItems()
    setWalls([...walls], walls[wallId].length = parseFloat(size))
    updateSquareMeters()
  }

  return (
    <React.Fragment>
    <h4>Measures</h4>
    <Collapse in={walls[wallId].sizeError}>
      <Alert severity="error">
        Maximum size <strong>{MAXIMUM_SIZE}m²</strong>
        <br />
        Minimum size <strong>{MINIMUM_SIZE}m²</strong>
      </Alert>
    </Collapse>
    <div className="input-div">
      <Input
        id="standard-adornment-weight"
        onChange={e => addHeight(e.target.value)}
        type="number"
        placeholder="Height"
        endAdornment={<InputAdornment position="end">M</InputAdornment>}
        aria-describedby="standard-weight-helper-text"
        inputProps={{
          'aria-label': 'weight',
        }}
      />
       <Collapse className="colapse" in={walls[wallId].heightError}>
          <span className="alert text-danger">
            * Minimum height {MINUMUM_HEIGHT}
          </span>
        </Collapse>
        <Input
          id="standard-adornment-weight"
          onChange={e => addLength(e.target.value)}
          type="number"
          placeholder="Length"
          endAdornment={<InputAdornment position="end">M</InputAdornment>}
          aria-describedby="standard-weight-helper-text"
          inputProps={{
            'aria-label': 'weight',
          }}
        />
        <br />
      </div>
    </React.Fragment>
  );
}
