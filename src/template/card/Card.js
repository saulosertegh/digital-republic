import React, { useEffect } from 'react'
import { useWallData } from '../../config/Walls'
import { MINUMUM_HEIGHT, MAXIMUM_SIZE, WINDOW_SIZE, DOOR_SIZE } from '../../config/Rules';
import Wall from './wall/Wall'
import Window from './window/Window'
import Door from './door/Door'
import './card.css'

export default function Card({ wallId }) {
    const { walls, setWalls } = useWallData();

    const amountOfPaintPerWall =
    walls[wallId].size - ((walls[wallId].windows * WINDOW_SIZE) + (walls[wallId].doors * DOOR_SIZE))

    useEffect(() => {
    setWalls([...walls], walls[wallId].paint = amountOfPaintPerWall)
    }, [walls[wallId].size, walls[wallId].availableSize, walls[wallId].windows, walls[wallId].doors])


  return (
    <div className={`card ${walls[wallId].sizeError && "border border-danger"}`}>
        <div className="card-body">
        <h2 className="card-title">Wall {wallId + 1}</h2>
        <h3>Size: {walls[wallId].size.toFixed(2)}m²</h3>
        <br />
        <Wall wallId={wallId} />
        <br />
        {walls[wallId].size >= MINUMUM_HEIGHT && walls[wallId].size <= MAXIMUM_SIZE && !walls[wallId].sizeError ?
          <React.Fragment>
            <h4>Add</h4>
            <Window wallId={wallId} />
            <Door wallId={wallId} />
            <br />
            <span>
              Available space: <strong>{walls[wallId].availableSize.toFixed(2)}m²</strong>
            </span>
          </React.Fragment> : ''
        }
      </div>
    </div>
  )
}
