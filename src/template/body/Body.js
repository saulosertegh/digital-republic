import React from 'react'
import { useWallData } from '../../config/Walls';
import Card from '../card/Card';
import Modal from '../modal/Modal';
import './body.css';

function Body() {
    const { walls } = useWallData();

    return (
        <div className="body">
        {walls.map(wall =>
          <Card wallId={wall.id} key={wall.id} />
        )}
        <div className="div-modal">
          <Modal />
        </div>
      </div>
    )
}

export default Body