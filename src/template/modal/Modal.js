import React from 'react';
import { useWallData } from '../../config/Walls'
import { makeStyles } from '@material-ui/core/styles';
import { MINUMUM_HEIGHT, PAINT_LITER, CANS } from '../../config/Rules';
import {Modal, Backdrop, Fade} from '@material-ui/core';
import './modal.css'

const useStyles = makeStyles((theme) => ({
    modal: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    paper: {
      backgroundColor: theme.palette.background.paper,
      border: '1px solid #000',
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
    },
  }));

export default function TransitionsModal() {
    const { walls, setWalls, totalWall, setTotalWall } = useWallData();
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);

    const handleOpen = () => {
        let open = true;
        for (let i in walls) {
          if (walls[i].size < MINUMUM_HEIGHT || walls[i].sizeError || walls[i].heightError) {
            setWalls([...walls], walls[i].sizeError = true)
            open = false;
          }
        }
        if (open) {
          setOpen(true);
        }
        addTotalWall()
      };

      const handleClose = () => {
        setOpen(false);
      };

      function addTotalWall() {
        let wallsTotal = 0;
    
        for (let i in walls) {
          wallsTotal += walls[i].paint;
          if (walls[i].size < MINUMUM_HEIGHT) {
            setWalls([...walls], walls[i].sizeError = true)
          }
        }
        let totalPaint = wallsTotal / PAINT_LITER;
        setTotalWall(totalPaint)
      }

  return (
    <div>
    <button type="button" onClick={handleOpen} className="btn btn-dark btn-lg btn-block">Calculate</button>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
        <div className={classes.paper}>
        <h2 id="transition-modal-title" className="alert btn-light info" role="alert">Paint total</h2>
        <p id="transition-modal-description">
            You will need <strong>{totalWall.toFixed(2)} liters</strong> of paint to color the room.
            </p>
        <ul className="list-group">
            <li className="list-group-item list-group-item-secondary">
            <h5 className="info">Can options</h5>
            </li>
            {CANS.map(can => (
            <li key={can.can} className="list-group-item">
                <h5>                
                {(
                    (totalWall / can.amount) > Math.round(totalWall / can.amount) ?
                    ` ${Math.round(totalWall / can.amount) + 1} ` :
                    ` ${Math.round(totalWall / can.amount)} `)
                }
                {(Math.round(totalWall / can.amount) > 1 ? 'Cans ' : 'Can ')}
                of {can.amount} Liters.
            </h5>
            </li>
            ))}
        </ul>
        </div>
    </Fade>
    </Modal>
    </div>
  );
}
